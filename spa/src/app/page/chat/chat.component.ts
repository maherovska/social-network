﻿import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Conversation } from "app/classes/conversation";
import { Message } from "app/classes/message";
import { UserProfile } from "app/classes/user-profile";
import { ChatService } from "app/services/chat.service";

@Component({
    selector: 'chat',
    templateUrl: './chat.html'
})

export class ChatComponent implements OnInit {
    conversations: Conversation[];
    filteredConversations: Conversation[];
    currentUsername: string;
    
    constructor(
        private router: Router,
        private chatService: ChatService) { }

    ngOnInit() {

        this.chatService.getLatestConversations()
            .subscribe(conversations => {
                this.conversations = conversations;
                this.filteredConversations = conversations;
            });    

        this.currentUsername = localStorage.getItem("username");        
    }

    onChatOpen(conversationId: number): void {  
        // localStorage.setItem("writeToUser", JSON.stringify(userProfile) );    
        this.router.navigate(['/', this.currentUsername, "chat", conversationId]);
    }

    search(val: string) {
        if (!val) this.filteredConversations = this.conversations;
        this.filteredConversations = this.conversations.filter(x => `${ x.userName.toLowerCase() } ${ x.userSurname.toLowerCase() }`.includes(val.toLowerCase()));
    }


    onUserProfile(username: any): void {
        this.router.navigate(["/", username]);
    }

}