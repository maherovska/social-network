namespace Socio.DataNew.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Conversations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        User_Id = c.Int(nullable: false),
                        Conversation_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .ForeignKey("dbo.Conversations", t => t.Conversation_Id)
                .Index(t => t.User_Id)
                .Index(t => t.Conversation_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        IsBanned = c.Boolean(nullable: false),
                        Role = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        Position = c.String(),
                        BirthDate = c.DateTime(),
                        Phone = c.String(),
                        ProfileImageUrl = c.String(),
                        WebsiteLink = c.String(),
                        FacebookLink = c.String(),
                        TwitterLink = c.String(),
                        City = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FeedMessages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FeedText = c.String(nullable: false),
                        WasEdited = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        ToUser_Id = c.Int(nullable: false),
                        FromUser_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.ToUser_Id)
                .ForeignKey("dbo.Users", t => t.FromUser_Id)
                .Index(t => t.ToUser_Id)
                .Index(t => t.FromUser_Id);
            
            CreateTable(
                "dbo.UserEntityConversationEntities",
                c => new
                    {
                        UserEntity_Id = c.Int(nullable: false),
                        ConversationEntity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserEntity_Id, t.ConversationEntity_Id })
                .ForeignKey("dbo.Users", t => t.UserEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.Conversations", t => t.ConversationEntity_Id, cascadeDelete: true)
                .Index(t => t.UserEntity_Id)
                .Index(t => t.ConversationEntity_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "Conversation_Id", "dbo.Conversations");
            DropForeignKey("dbo.FeedMessages", "FromUser_Id", "dbo.Users");
            DropForeignKey("dbo.FeedMessages", "ToUser_Id", "dbo.Users");
            DropForeignKey("dbo.Messages", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserEntityConversationEntities", "ConversationEntity_Id", "dbo.Conversations");
            DropForeignKey("dbo.UserEntityConversationEntities", "UserEntity_Id", "dbo.Users");
            DropIndex("dbo.UserEntityConversationEntities", new[] { "ConversationEntity_Id" });
            DropIndex("dbo.UserEntityConversationEntities", new[] { "UserEntity_Id" });
            DropIndex("dbo.FeedMessages", new[] { "FromUser_Id" });
            DropIndex("dbo.FeedMessages", new[] { "ToUser_Id" });
            DropIndex("dbo.Messages", new[] { "Conversation_Id" });
            DropIndex("dbo.Messages", new[] { "User_Id" });
            DropTable("dbo.UserEntityConversationEntities");
            DropTable("dbo.FeedMessages");
            DropTable("dbo.Users");
            DropTable("dbo.Messages");
            DropTable("dbo.Conversations");
        }
    }
}
